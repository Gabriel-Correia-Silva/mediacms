## Instalação do server
Executar como root

```bash
mkdir /home/mediacms.io && cd /home/mediacms.io/
git clone https://gitlab.com/Gabriel-Correia-Silva/mediacms.git
cd /home/mediacms.io/mediacms/ && bash ./install.sh
```
Apos digite yes e quando perguntar qual url apenas pressione enter e portal name pressione enter novamente
## Iniciar

```bash
docker-compose up
```
A aplicação será executado em http://localhost/.
## Teste
Caso queira testar localmente execute:
```bash
name: Build the Stack
      run:  docker-compose -f docker-compose-dev.yaml build

name: Start containers
      run: docker-compose -f docker-compose-dev.yaml up -d

name: List containers
      run: docker ps

name: Run Django Tests
      run: docker-compose -f docker-compose-dev.yaml exec --env TESTING=True -T web pytest

name: Tear down the Stack
      run:  docker-compose -f docker-compose-dev.yaml down
```

